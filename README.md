# IONIC PIZZA #

This idea came up from New Inn Pizza just up here in Auckland, New Zealand. so thought to create the demo app. 

### Description ###

I use Ionic framework and below front end technologies to build Hybrid app.


1. **HTML**


2. **CSS**


3. **AngularJS**



### Screen Shots ###
## Pizza Home Screen ##
bellow tab(buttons) will do appropriate job, like dialing a phone, making order, emailing (Abstract).

![HomePizzaMenu.png](https://bitbucket.org/repo/zkaxBM/images/883035248-HomePizzaMenu.png)

## Pizza Order Screen ##
User can give name to pizza and select the topping.

![OrderPizza.png](https://bitbucket.org/repo/zkaxBM/images/1201702063-OrderPizza.png)

### Pizza Checkout ###
Checkout screen

![PizzaCheckout.png](https://bitbucket.org/repo/zkaxBM/images/3179768241-PizzaCheckout.png)


App ID: 367A5419 (Ionic View app require to check on mobile)

Hope you are not feeling hungry with Ionic framework, feel free to tweaks.

Cheers
K